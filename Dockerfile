# stage 1 : build
FROM node:16.13.0-alpine as builder

COPY . /app
WORKDIR /app
RUN npm install

# build production version
RUN npm run prod

# stage 2 : run
FROM nginx:1.17.10-alpine
EXPOSE 89
COPY --from=builder /app/dist /usr/share/nginx/html
